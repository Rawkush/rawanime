class Episode{
  final String episodeNumber, episodeUrl, thumbnail;
  Episode(this.episodeNumber, this.episodeUrl, this.thumbnail);
}